# vue-covid-tracker
A web app to check covid 19 cases across the globe via an API

............Features...........
-simple interface
-simple design
-very responsive
-Good user experience

![windows1](https://user-images.githubusercontent.com/55124189/126885575-d409faf1-5e4d-470e-820a-d18e970b8aa8.jpg)
![windows2](https://user-images.githubusercontent.com/55124189/126885578-a417131f-0cfc-4595-b621-165dc578cfd2.jpg)
![windows3](https://user-images.githubusercontent.com/55124189/126885579-13d9f858-03f4-4890-852d-580f1ccbc49b.jpg)
![windows4](https://user-images.githubusercontent.com/55124189/126885580-478966c9-1abb-482c-adc6-83053fd30393.jpg)
